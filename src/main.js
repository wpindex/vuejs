import Vue from "vue";
import './plugins/vuetify'
import Vuetify from 'vuetify/lib'
import App from "./App.vue";
import BootstrapVue from "bootstrap-vue"
import "bootstrap/dist/css/bootstrap.css"
import "bootstrap-vue/dist/bootstrap-vue.css"
import router from "./router";
import VueResource from 'vue-resource'
import Auth from './packages/auth/auth.js'
//import moment from 'moment'
import moment from 'moment/src/moment'

import VueGoodTablePlugin from 'vue-good-table';


require('./assets/css/style.css') //Call your custom stylesheet
import 'vue-good-table/dist/vue-good-table.css'

Vue.use(BootstrapVue)
Vue.use(Vuetify)
Vue.use(VueResource)
Vue.use(Auth)
Vue.prototype.moment = moment
Vue.use(VueGoodTablePlugin);

Vue.http.options.root = 'https://profile.sevaportal.com/'
Vue.http.headers.common['Authorization'] = 'Bearer ' + Vue.auth.getToken()

Vue.config.productionTip = false;



router.beforeEach(
  (to, from, next) => {
    if(to.matched.some(record => record.meta.forVisitors)) {
      if(Vue.auth.isAuthenticated()) {
        next ({
          path: '/'
        })
      } else next()
    } 
    
    else if(to.matched.some(record => record.meta.forAuth)) {
      if( ! Vue.auth.isAuthenticated()) {
        next ({
          path: '/login'
        })
      } else next()
    } 
    
    else next()
  }
)

new Vue({
  router,
  render: function(h) {
    return h(App);
  }
}).$mount("#app");
