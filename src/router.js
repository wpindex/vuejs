import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import ProfessionCreate from "./components/profession/create"
import ProfessionList from "./components/profession/list"
import ProfesionUpdate from "./components/profession/update.vue"
import OrganizationCreate from "./components/organization/create"
import OrganizationList from "./components/organization/list"
import OrganizationUpdate from "./components/organization/update.vue"
import CategoryCreate from "./components/category/create"
import CategoryList from "./components/category/list"
import CategoryUpdate from "./components/category/update.vue"
import SeekerCreate from "./components/seeker/create"
import SeekerList from "./components/seeker/list"
import SeekerUpdate from "./components/seeker/update.vue"
import StatusCreate from "./components/status/create"
import StatusList from "./components/status/list"
import StatusUpdate from "./components/status/update.vue"
import MemberDetails from "./components/member/view.vue"
import LocationCreate from "./components/location/create"
import LocationList from "./components/location/list"
import LocationUpdate from "./components/location/update.vue"
import UserCreate from "./components/user/create"
import UserList from "./components/user/list"
import UserUpdate from "./components/user/update.vue"

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "*",
      name: "home",
      component: Home,
      meta: {
        forAuth: true
      },
    },
    {
      path: "/register",
      name: "register",
      meta: {
        //forVisitors: true
      },
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: function() {
        return import(/* webpackChunkName: "about" */ "./views/Register.vue");
      }
    },
    {
      path: "/member/list/:page",
      name: "members",
      meta: {
        forAuth: true
      },
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: function() {
        return import(/* webpackChunkName: "about" */ "./components/member/list.vue");
      }
    },
    {
      path: "/member/:member/view",
      meta: {
        forAuth: true
      },
      component: MemberDetails
    },
    {
      path: "/login",
      name: "login",
      meta: {
        forVisitors: true
      },
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: function() {
        return import(/* webpackChunkName: "about" */ "./views/Login.vue");
      }
    },
    {
      path: "/logout",
      name: "logout",
      meta: {
        forAuth: true
      },
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: function() {
        return import(/* webpackChunkName: "about" */ "./views/Logout.vue");
      }
    },
    {
      path: "/profession/create",
      component: ProfessionCreate,
      meta: {
        forAuth: true
      },
    },
    {
      path: "/profession/list",
      component: ProfessionList,
      meta: {
        forAuth: true
      },
    },
    {
      path: "/profession/:profession/update",
      component: ProfesionUpdate,
      meta: {
        forAuth: true
      },
    },
    {
      path: "/organization/create",
      component: OrganizationCreate,
      meta: {
        forAuth: true
      },
    },
    {
      path: "/organization/list",
      component: OrganizationList,
      meta: {
        forAuth: true
      },
    },
    {
      path: "/organization/:organization/update",
      component: OrganizationUpdate,
      meta: {
        forAuth: true
      },
    },
    {
      path: "/category/create",
      component: CategoryCreate,
      meta: {
        forAuth: true
      },
    },
    {
      path: "/category/list",
      component: CategoryList,
      meta: {
        forAuth: true
      },
    },
    {
      path: "/category/:category/update",
      component: CategoryUpdate,
      meta: {
        forAuth: true
      },
    },
    {
      path: "/seeker/create",
      component: SeekerCreate,
      meta: {
        forAuth: true
      },
    },
    {
      path: "/seeker/list",
      component: SeekerList,
      meta: {
        forAuth: true
      },
    },
    {
      path: "/seeker/:seeker/update",
      component: SeekerUpdate,
      meta: {
        forAuth: true
      },
    },
    {
      path: "/status/create",
      component: StatusCreate,
      meta: {
        forAuth: true
      },
    },
    {
      path: "/status/list",
      component: StatusList,
      meta: {
        forAuth: true
      },
    },
    {
      path: "/status/:status/update",
      component: StatusUpdate,
      meta: {
        forAuth: true
      },
    },
      {
        path: "/location/:parent/create",
        name: 'LocationCreate',
        component: LocationCreate,
        meta: {
          forAuth: true
        },
      },
      {
        path: "/location/list",
        component: LocationList,
        meta: {
          forAuth: true
        },
      },
      {
        path: "/location/:location/update",
        component: LocationUpdate,
        meta: {
          forAuth: true
        }    
    },
    {
      path: "/user/create",
      name: 'UserCreate',
      component: UserCreate,
      meta: {
        forAuth: true
      },
    },
    {
      path: "/user/list",
      component: UserList,
      meta: {
        forAuth: true
      },
    },
    {
      path: "/user/:user/update",
      component: UserUpdate,
      meta: {
        forAuth: true
      },
    }
    
    
  ]
});
